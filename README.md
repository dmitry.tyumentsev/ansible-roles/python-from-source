Python from sorce role
=========

Build and Install Python from source code on Debian/Ubuntu or Fedora/RHEL servers.

Example Playbook
----------------
Docker executor with Docker-in-Docker
```yml
- hosts: all
  become: true
  roles:
    - role: tyumentsev4.python_from_source
  vars:
    python_release_version: 3.11.5
```
